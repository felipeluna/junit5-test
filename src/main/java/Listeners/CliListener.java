package Listeners;

import org.junit.platform.engine.TestExecutionResult;
import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestIdentifier;

/**
 * Created by felipe on 08/03/18.
 */
public class CliListener implements TestExecutionListener {
    @Override
    public void executionStarted(TestIdentifier testIdentifier) {
        if (testIdentifier.isTest()) {
            System.out.println(testIdentifier.getDisplayName() + " is starting");
        }
    }

    @Override
    public void executionFinished(TestIdentifier testIdentifier, TestExecutionResult testExecutionResult) {
        if (testIdentifier.isTest()) {
                System.out.println(testIdentifier.getDisplayName() + " " + testExecutionResult.getStatus());
            System.out.println(testExecutionResult.getThrowable().get().getMessage());
        }
    }


}
