package com.example.demo;

import Listeners.CliListener;
import com.example.demo.com.example.demo.tests.TestTest;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestPlan;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.LoggingListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.util.logging.Level;

import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Component
	public class CommandLineAppStartupRunner implements CommandLineRunner {
		private final Logger logger = LoggerFactory.getLogger(CommandLineAppStartupRunner.class);

		@Override
		public void run(String... args) throws Exception {
            System.out.println("test");
            LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
					.selectors(
							selectClass(TestTest.class)
					)
					.build();

			Launcher launcher = LauncherFactory.create();
            TestPlan testPlan = launcher.discover(request);



            TestExecutionListener listener = LoggingListener.forJavaUtilLogging(Level.INFO); //new LoggingListener();

//            LoggingListener loggingListener = new LoggingListener();
            launcher.execute(request, new CliListener());


		}
	}
}
