package com.example.demo.com.example.demo.tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TestTest {

	@Test
    @DisplayName("First test")
	public void teste() {
		assertEquals("hi", "ho", "the greetings are different");
	}

	@Test
    @DisplayName("Second Test")
	public void secondTest() {
        assertNull(null, "this should be null");
    }

}
