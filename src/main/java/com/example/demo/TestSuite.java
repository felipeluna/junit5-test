package com.example.demo;

import com.example.demo.com.example.demo.tests.TestTest;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

/**
 * Created by felipe on 08/03/18.
 */
@SelectClasses(TestTest.class)
public class TestSuite {}
